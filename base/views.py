from django.shortcuts import render

def index(request):
	template_name = 'base.html'
	data = {}
	return render(request, template_name, data)

#Modificar y agregar para limitar vistas a usuarios
#def user_can_vote(user):
#	return user.is_authenticated() and user.has_perm("polls.can_vote")

#@user_passes_test(user_can_vote, login_url="/login/")

def base_inicio(request):
	template_name = 'base_lod.html'
	data = {}
	return render(request, template_name, data)