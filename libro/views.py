from django.shortcuts import render

def anotacion(request):
    template_name = 'anotacion.html'
    data = {}

    return render(request, template_name, data)