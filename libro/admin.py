from django.contrib import admin
from .models import *

class LibroAdmin(admin.ModelAdmin):
	search_fields = ['folio','id_contrato']
	list_display = ('folio','id_contrato','nombre_empresa_contratista','rut_empresa_contratista',)

class TipoLibroAdmin(admin.ModelAdmin):
	search_fields = ['id_tipo_libro','nombre_tipo_libro']
	list_display = ('id_tipo_libro','nombre_tipo_libro',)

admin.site.register(TipoLibro)
admin.site.register(Libro)
#admin.site.register(LibroUsuario)