from django.db import models
#from usuario.models import User

class TipoLibro(models.Model):
	id_tipo_libro = models.IntegerField()
	nombre_tipo_libro = models.CharField(max_length=60)
	nombre_tipo_comunicacion = models.CharField(max_length=60, null=True)
	permiso_comunicacion = models.IntegerField()

	def __str__(self):
		return self.id_tipo_libro

class Libro(models.Model):
	folio = models.IntegerField(primary_key = True, null=False, blank = False)
	#tipo_libro = models.ForeignKey(TipoLibro, null=False, blank=False, on_delete=models.CASCADE)
	id_contrato = models.IntegerField()
	nombre_contrato = models.CharField(max_length=144)
	direccion = models.CharField(max_length=60)
	tipo_contrato = models.CharField(max_length=60)
	modalidad_contratacion = models.CharField(max_length=60)
	numero_res_dec_adju = models.IntegerField('Número de resolución o decreto adjuditorio')
	start_date = models.DateTimeField('Fecha resolución o decreto adjuditorio',auto_now_add=False)
	plazo_inicial_contrato = models.CharField(max_length=60)
	start_date_contrato = models.DateTimeField('Fecha inicio contrato',auto_now_add=False)
	end_date_contrato = models.DateTimeField('Fecha termino contrato',auto_now_add=False)
	tipo_moneda = models.CharField(max_length=60)
	monto_inicial = models.IntegerField()
	monto_vigente = models.IntegerField()
	rut_empresa_contratista = models.CharField(max_length=13)
	nombre_empresa_contratista = models.CharField(max_length=60)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)
	active = models.BooleanField(default=True)

	def __str__(self):
		return str(self.folio)

#class LibroUsuario(models.Model):
#	libro_user = models.ForeignKey(Libro, null=False, blank=False, on_delete=models.CASCADE)
#	usuario_libro = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)

#	def __str__(self):
#		return self.libro_user