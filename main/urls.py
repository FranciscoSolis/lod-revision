from django.contrib import admin, auth
from django.urls import path, include
from django.contrib.auth.views import logout_then_login

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('base.urls')),
    path('accounts/login/', include('base.urls')),
    path('accounts', include('django.contrib.auth.urls')),
    path('anotacion/', include('libro.urls')),
    path('logout/', logout_then_login)
]
