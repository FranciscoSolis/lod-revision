from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser
from django.db import models

class User(AbstractUser):
    contratista = models.BooleanField(default=False)
    inspector_fiscal = models.BooleanField(default=False)
    externo = models.BooleanField(default=False)

    def get_contratista(self):
        contratista_perfil = None
        if hasattr(self, 'ContratistaPerfil'):
            contratista_perfil = self.ContratistaPerfil
        return contratista_perfil

    def get_inspector_fiscal(self):
        inspector_fiscal_perfil = None
        if hasattr(self, 'InspectorFiscalPerfil'):
            inspector_fiscal_perfil = self.InspectorFiscalPerfil
        return inspector_fiscal_perfil

    def get_externo(self):
        externo_perfil = None
        if hasattr(self, 'ExternoPerfil'):
            externo_perfil = self.ExternoPerfil
        return externo_perfil

    class Meta:
        db_table = 'auth_user'

class ContratistaPerfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    name = models.CharField(max_length=64)


class InspectorFiscalPerfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    name = models.CharField(max_length=64)


class ExternoPerfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    name = models.CharField(max_length=64)